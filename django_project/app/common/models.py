from django.contrib.auth import get_user_model
from django.db import models

DEFAULT_DATE_FORMAT = '%Y-%m-%d'

class AuditModel(models.Model):
    created_by = models.ForeignKey(get_user_model(), null=True, blank=True,on_delete=models.PROTECT)
    created_date = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated_date = models.DateTimeField(auto_now=True, null=True, blank=True)

    class Meta:
        abstract = True
