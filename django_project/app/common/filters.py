import json

QUERY_TYPE = {
    '=': 'exact',
    'like' : 'icontains',
    '<=' : 'lte',
    '>=': 'gte'
}

def apply_filters(queryset, dynamic_filters):
    for filter in dynamic_filters:        
        if not filter['value']:
            continue

        search_mode = QUERY_TYPE[filter['mode']]
        query_field = '{}__{}'.format(filter['field'], search_mode)
        queryset = queryset.filter(**{query_field: filter['value']})

    return queryset 


def apply_static_filter(request, queryset, field):
    search_filter = request.GET.get('search')
    status_filter = request.GET.get('status')
    filters = []

    if search_filter:
        filters.append({
            'field': field['field'],
            'value': search_filter,
            'mode': 'like'
        })

    if status_filter:
        filters.append({
            'field': 'is_active',
            'value': status_filter,
            'mode': '='
        })
    
    return apply_filters(queryset, filters)

def apply_dynamic_filter(request, queryset):    
    dynamic_filters = request.GET.getlist('dynamic_filters[]')
    return apply_filters(queryset, [ json.loads(filter) for filter in dynamic_filters] )

