from rest_framework import permissions
from rest_framework import authentication
import app.settings as settings
from django.contrib.auth.models import User
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
import jwt

__SECRET_KEY = settings.SECRET_KEY
__ALGORITHM = settings.TOKEN['algorithm']
__ISSUER = settings.TOKEN['issuer']
__ENCODING = 'ascii'

class WhitelistPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        ip_address = request.META['REMOTE_ADDR']

        if '*' in settings.ALLOWED_HOSTS:
            return True
            
        return ip_address in settings.ALLOWED_HOSTS


def get_token_from_header(request):
    authorization_header = request.META['HTTP_AUTHORIZATION']

    if not authorization_header.startswith(settings.TOKEN['type']):
        raise ValueError(f"Not {settings.TOKEN['type']} token")

    return authorization_header.split(' ')[-1]


def decode_token(token, use_exp=True):
    token_decoded = jwt.decode(token, __SECRET_KEY,
                               algorithms=[__ALGORITHM],
                               verify=use_exp)

    return token_decoded

class BaseTokenAuthentication(authentication.BaseAuthentication):

    def authenticate(self, request):
        try:
            token = get_token_from_header(request)

            return self.token_authentication(request, token)

        except (KeyError, ValueError, ExpiredSignatureError, InvalidSignatureError, ObjectDoesNotExist):
            return None, None

    def token_authentication(self, request, token):
        pass


class AccessTokenAuthentication(BaseTokenAuthentication):

    def has_permission(self, request, view):
        token = get_token_from_header(request)
        token_data = decode_token(token)

        if not 'username' in token_data:
            return False
        
        user = User.objects.filter(username = token_data['username'], id = token_data['user_id']).first()

        if not user:
            return False

        return True

