from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.views import TokenObtainPairView
import app.settings as settings
import time
import jwt

__SECRET_KEY = settings.SECRET_KEY
__ALGORITHM = settings.TOKEN['algorithm']
__ISSUER = settings.TOKEN['issuer']
__ENCODING = 'ascii'

def __generate_message(data, use_exp=True):
    issue_time = int(time.time())
    message = {
        'iss': __ISSUER,
        'sub': data,
        'iat': issue_time
    }

    if use_exp:
        expiration_time = settings.TOKEN['expiration_time']  # Expiration time in second

        if not expiration_time == 0:
            expiration_time = (issue_time + expiration_time)
            message['exp'] = expiration_time

    return message


def __generate_token(data, use_exp=True):
    message = __generate_message(data, use_exp=use_exp)
    token = jwt.encode(message, __SECRET_KEY, algorithm=__ALGORITHM)

    return token.decode(__ENCODING)

def generate_access_token(data):
    return __generate_token(data)

class CustomTokenObtainPairSerializer(TokenObtainPairSerializer):
    @classmethod
    def get_token(cls, user):
        token = super(CustomTokenObtainPairSerializer, cls).get_token(user)
        token['username'] = user.username
        return token

    def validate(self, attrs):
        data = super().validate(attrs)
        refresh = self.get_token(self.user)
        data['refresh'] = str(refresh)
        data['username'] = self.user.username
        data['last_access'] = self.user.last_login.strftime(r'%Y-%m-%d %H:%M:%S') if self.user.last_login else ''
        #data['groups'] = self.user.groups.values_list('name', flat=True)
        return data

class CustomTokenObtainPairView(TokenObtainPairView):
    serializer_class = CustomTokenObtainPairSerializer