from apps.core.models import EmailPool, EmailLayout

def email(mail_options, template_name, **values):
    # Getting mail template
    layout = EmailLayout.objects.get(layout_name=template_name)
    
    if not layout:
        return False

    mail = EmailPool()
    mail.send(layout, mail_options, **values)
