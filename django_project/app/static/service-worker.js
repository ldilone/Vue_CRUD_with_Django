importScripts("/static/precache-manifest.dba906892c5236eb9bac4e1027c87ed7.js", "https://storage.googleapis.com/workbox-cdn/releases/3.6.3/workbox-sw.js");

// eslint-disable-next-line
if (workbox) {
    console.log(`Workbox is loaded`);
    // eslint-disable-next-line
    workbox.precaching.precacheAndRoute(self.__precacheManifest);

} 
else {
    console.log(`Workbox didn't load`);
}

