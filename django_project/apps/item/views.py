from rest_framework import generics
from rest_framework.response import Response
from .models import *
from .serializers import *
from rest_framework.permissions import IsAuthenticated
from app.common.filters import apply_dynamic_filter, apply_static_filter
import django_filters.rest_framework

class ItemList(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Item.objects.all()
    serializer_class = ItemSerializer
    
    def get_queryset(self,*args, **kwargs):    
        queryset = super(ItemList, self).get_queryset(*args, **kwargs)
        queryset = apply_static_filter(self.request, queryset, {'field': 'name'})      
        queryset = apply_dynamic_filter(self.request, queryset)      
        return queryset  
      
class ItemListId(generics.RetrieveUpdateAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Item.objects.all()
    serializer_class = ItemSerializer  
