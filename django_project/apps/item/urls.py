from django.urls import path

from .views import *

urlpatterns = [
    path('', ItemList.as_view(),name='item_list'),
    path('<int:pk>', ItemListId.as_view(),name='item_id'),    
]
