from django.db import models

class Item(models.Model):
    class Meta:
        verbose_name = 'items'

    name = models.CharField(max_length=255)
    quantity = models.DecimalField(max_digits=20, decimal_places=2)
    unit_cost = models.DecimalField(max_digits=20, decimal_places=2)
