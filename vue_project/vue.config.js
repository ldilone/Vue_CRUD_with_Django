const path = require('path');

module.exports = {
    publicPath: process.env.NODE_ENV === 'production'
      ? '/static/'
      : '/',
    pwa: {
        // configure the workbox plugin
        workboxPluginMode: 'InjectManifest',
        workboxOptions: {
                // swSrc is required in InjectManifest mode.
                swSrc: 'public/service-worker.js',
                // ...other Workbox options...
        }
    },
    outputDir: path.resolve(__dirname, '../django_project/app/static')
  }