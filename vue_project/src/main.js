//Importing prototypes
import '@/helpers/prototypes';

import Vue from "vue"
import App from "./App.vue"
import router from "./router/"
import store from "@/store/"
import vuetify from '@/plugins/vuetify'
import filters from '@/helpers/filters'
import RouterSecurity from '@/store/modules/auth/router_security'

import "./registerServiceWorker"
import '@/mixins/general_mixin'
import '@/plugins'

//Importing languages
import {i18n} from '@/i18n'

Vue.config.productionTip = false;

//Adding filters to Vue
filters.setGlobalFilters(Vue);

//Setting up router security
router.beforeEach((to, from, next) => {    
    RouterSecurity.checkUserAuth(to, from, next, store);
    window.scrollTo(0, 0);
});

new Vue({
    router,
    store,
    vuetify,
    beforeCreate() {
		this.$store.dispatch('auth/loadInitialData');
    },
    i18n,
    render: h => h(App)
}).$mount("#app");
