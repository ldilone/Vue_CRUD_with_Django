export default {
    SYSTEM_MESSAGES: {
        //Defining messages types
        MSG_TYPES: {
			SUCCESS: {
                icon: 'mdi-close-circle',
                type: 'success',
            },
			ERROR: {
                icon: 'mdi-close-circle',
                type: 'error',
            },
            WARNING: {
                icon: 'mdi-close-circle',
                type: 'warning'
            }         
        },
        
        LOGIN_FAILED: {
            msg: 'login_failed',
            type: 'ERROR'
        },

        //General app messages
        SAVE_SUCCESS: {
            msg: 'record_saved_successful',
            type: 'SUCCESS'
        },

        PASSWORD_CHANGE_SUCCESS: {
            msg: 'password_change_success',
            type: 'SUCCESS'
        },

        ERROR_CHANGIN_PASSWORD: {
            msg: 'error_changin_password',
            type: 'ERROR'
        },

        SAVE_ERROR: {
            msg: 'saving_error',
            type: 'ERROR'
        },

        DEACTIVATE_SUCCESS: {
            msg: 'deactivate_successful',
            type: 'SUCCESS'
        },

        DEACTIVATE_SUCCESS_PLURAL: {
            msg: 'deactivate_successful_plural',
            type: 'SUCCESS'
        },

        DEACTIVATE_ERROR: {
            msg: 'deactivate_error',
            type: 'ERROR'
        },

        ERROR_LOADING_DATA: {
            msg: 'error_loading_data',
            type: 'ERROR'
        },

        QTY_GREATER_THAN_CERO: {
            msg: 'qty_greater_than_cero',
            type: 'WARNING'
        },

        UNIT_COST_GREATER_THAN_CERO: {
            msg: 'unit_cost_greater_than_cero',
            type: 'WARNING'
        },
        
        PASSWORD_NOT_MATCH: {
            msg: 'password_not_match',
            type: 'WARNING'
        },

        FILL_FORM: {
            msg: 'please_fill_form',
            type: 'WARNING'
        },

        NO_DATA_FOUND: {
            msg: 'no_data_found',
            type: 'SUCCESS'
        },

        EMAIL_NOT_MATCH: {
            msg: 'email_not_match',
            type: 'SUCCESS'
        },

        SELECT_ONE_RECORD: {
            msg: 'select_one_record',
            type: 'WARNING'
        },

        MUST_SPECIFY_PASSWORD: {
            msg: 'must_specify_password',
            type: 'WARNING'
        },

        //Setting up confirm messages
        CONFIRM_MESSAGES: {
            SAVE_CONFIRM: 'save_confirmation',
            LOGOUT_CONFIRM: 'confirm_logout',
            SURE_CANCEL: 'sure_cancel',
            DEACTIVATE_CONFIRM: 'deactivate_confirm',
            DELETE_CONFIRM: 'delete_confirm',
            DELETE_CONFIRM_PLURAL: 'delete_confirm_plural'
        }
    }
}