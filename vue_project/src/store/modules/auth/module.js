import INITIAL_STATE from './initial_state'
import menu_options from './menu_options'
import API from './api'
import security from "./router_security"
import router from '../../../router'

const TOKEN_REFRESH_INTERVAL = 30; //Interval in seconds
var timer = null;

export function getRouterOptions() {
    return router;
}

export const state = {
    user_menu_options: [],
    user_roles: JSON.parse(localStorage.getItem('security_roles')) || [],
    logged_user: JSON.parse(localStorage.getItem('user')) || {},
    session_token: JSON.parse(localStorage.getItem('session_token')) || null
};

export const getters = {

};

export const mutations = {
    setUserDomains (state, domains) {
        state.user_domains = domains;
        if(domains.length == 1){
            state.current_domain = domains[0].domain_url;
            localStorage.setItem('current_domain',  state.current_domain);
        }
    },

    setCurrentDomain (state, domain) {
        state.current_domain = domain;
        localStorage.setItem('current_domain', domain);
    },

    setLoggedUser(state, login_data) {
        const user = {
            username: login_data.username,
            roles: login_data.roles || []
        };

        const token = {
            refresh: login_data.refresh,
            access: login_data.access
        };

        localStorage.setItem('user', JSON.stringify(user));
        localStorage.setItem('session_token', JSON.stringify(token));
        state.logged_user = user;
        state.session_token = token;
    },

    setAccessToken(state, token) {
        state.session_token.access = token.access;
        localStorage.setItem('session_token', JSON.stringify(state.session_token));
    },

    setMenuOptions (state, menu_options) {
        state.user_menu_options = menu_options;
    },
    
    setUserRoles (state, roles) {
        state.user_roles = roles;
        localStorage.setItem('security_roles', JSON.stringify(roles));
    },

    logoutUser(state) {
        localStorage.removeItem('user');
        localStorage.removeItem('session_token');
        state.logged_user = {};
    },
};

export const actions = {

    logOut({ commit }) {
        commit('logoutUser')
        window.location = '/';
        return;
    },

    /**
     * Getting the domains associated to the user
     * @param {*} username - user email associated to the domain table
    */  
    async getUserDomains ({ commit }, username) {
        const domains = await API.getUserDomains(username).catch(()=>{});
        if(domains) commit('setUserDomains', domains)
    },

    /**
     * Refresh token method
     * @param {*} refresh_token - storage token used to refresh access token
     */
    async refreshToken({ dispatch, commit}, refresh_token) {
        const response = await API.refreshToken({
            refresh: refresh_token
        }).catch(() => {});

        if (!response) {
            dispatch('logoutUser');
            return;
        }

        commit('setAccessToken', response);
    },

    /**
     * Managin refresh token
     */
    scheduleRefresh({ dispatch, state }) {
        if (!state.session_token) return;

        timer = setInterval(() => {
            var session_data = state.session_token;
            //Stop refresh token if user is not logged
            if (!session_data.refresh) {
                clearInterval(timer);
                dispatch('logoutUser');
                window.location = '/login';
                return;
            }

            dispatch('refreshToken', session_data.refresh);
        }, TOKEN_REFRESH_INTERVAL * 1000);
    },
    
    async login({ commit, dispatch }, user) {
        const response = await API.login(user).catch(() => {});

        if (!response || !response.access)
            return Promise.reject(false);

        commit('setLoggedUser', response);
        dispatch('drawMenu', menu_options.MENU);
        return Promise.resolve(true);
    },

    /**
     * Getting all user and group user permissions
     */
    async getUserPermissions ({commit}) {
        const response = await API.getUserPermissions().catch(()=>{});

        if(!response) 
            return Promise.reject(false);
        
        //Loading user roles
        commit('setUserRoles', response);
        return Promise.resolve(response);
    },

    drawMenu ({state, commit}, menu_options) {
        var menu = security.menuValidator(
            menu_options,
            router.options.routes, 
            state.user_roles
        ).getMenu();
        
        commit('setMenuOptions', menu);
    },

    /**
     * Changing password
    */
    changePassword ({}, user_data) {
        return API.changePassword(user_data);
    },

    /**
     * Loading initial information
    */
	loadInitialData ({dispatch, state}) {                     
        if(state.session_token){
            dispatch('refreshToken', state.session_token.refresh);
            dispatch('drawMenu', menu_options.MENU);
            dispatch('scheduleRefresh');
        }
    },    
};
