import http_request from '@/store/api'

const ENDPOINT = http_request.default_endpoint;

export default {
    login (user) {
        return http_request.post(ENDPOINT + 'api/login/', user);        
    },

    refreshToken (token) {
        return http_request.post(ENDPOINT + 'api/token/refresh/', token);        
    },

    verifyUser: () => {
		return http_request.post(ENDPOINT + 'users/verifyUser', {});
    },

    getMenuOptions (domain_url, module_id) {
        return http_request.get(ENDPOINT + 'api/access/menu_options/{0}/{1}'.format(domain_url, module_id));   
    },

    getUserPermissions (user_id = null) {
        return http_request.get(ENDPOINT + 'api/access/user/permissions/' + (user_id || ''));   
    },
    
}
