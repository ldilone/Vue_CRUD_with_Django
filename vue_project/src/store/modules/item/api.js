import http_request from '@/store/api'

const ENDPOINT = http_request.default_endpoint;
const API_URL = ENDPOINT + 'api/item/';

export default {    
    getAll (params) {
        const offset = (params.page -1) * params.itemsPerPage;
        const order_by = Array.zip(params.sortBy, params.sortDesc);
        //Avoid query filter status when all status are selected or none is selected
        const filter_status = params.static_filters.selected_status.length == 1 ? params.static_filters.selected_status[0] : null;

        //Defining query filters
        return http_request.get(API_URL, {
            limit: params.itemsPerPage,
            offset: offset,
            search: params.static_filters.search,
            status: filter_status,
            dynamic_filters: params.dynamic_filters,
            order_by: order_by
        });
    },

    save: (record) => {
        if(record.id)
    		return http_request.put(API_URL + record.id, record);
        else
	    	return http_request.post(API_URL, record);
    },

    get (id) {
        return http_request.get(API_URL + id);        
    },

    deactivateRecords: (records) => {
        return http_request.patch(API_URL, records);
    }
}
