import INITIAL_STATE from './initial_state'
import API from './api'

export const state = {
    //Main form config
    form: Object.clone(INITIAL_STATE.form),

    //Config for grid-view
    records_config: {
        grid_headers: INITIAL_STATE.grid_headers,
        records: [],
        total_rows: 0,
        selected: [],
        general_config: Object.clone(INITIAL_STATE.grid_config)
    },

    summary: {quantity: 0, unit_cost: 0, total_amount: 0}
};

export const getters = {
    //Getting initial filters of main grid
    getInitialFilters () {
        return {
            static: INITIAL_STATE.grid_config.static_filters,
            dynamic: INITIAL_STATE.grid_config.dynamic_filters
        };
    }
};

export const mutations = {
    //Cleaning form inputs with the initial values
    clearForm (state) {
        state.form = Object.clone(INITIAL_STATE.form);
    },

    setSummary (state, summary) {
        state.summary = summary;
    }
};

export const actions = {
    async getAll ({commit, state}, grid_config) {
        var search_params = Object.clone(grid_config || state.records_config.general_config);
        
        commit('setGridGeneralConfig', search_params);        
        const response = await API.getAll(search_params).catch((e)=>{ return Promise.reject(e) });
        
        var summary = {quantity: 0, unit_cost: 0, total_amount: 0};
        response.results = response.results.map((e)=>{
            e.quantity = parseInt(e.quantity);
            e.format_unit_cost = (e.unit_cost).format();
            e.total_amount = (e.quantity * parseFloat(e.unit_cost)).format();

            summary = {
                quantity: summary.quantity + e.quantity,
                //unit_cost: summary.unit_cost + e.unit_cost,
                unit_cost: summary.unit_cost + parseFloat(e.unit_cost),
                total_amount: summary.total_amount + (e.quantity * e.unit_cost)
            }
            return e;
        });

        //Setting records_set
        commit('setRecords', response);
        commit('setSummary', summary);
        return Promise.resolve(response);
    },

    async get ({commit}, id) {
        //Getting record
        const response = await API.get(id).catch((e)=>{ return Promise.reject(e) });

        if(response){
            response.quantity = parseInt(response.quantity);
            commit('loadEntity', response);
            return Promise.resolve(response);
        }
    },

    async save ({ state }) {
        //Saving record
        const response = await API.save(state.form).catch((e)=>{ return Promise.reject(e) });
        if(response) return Promise.resolve(response);
    },

    async deactivateRecords ({ dispatch, state }) {
        const records = state.records_config.selected.map((e)=>{ return e.id });
        //Deleting record
        const response = await API.deactivateRecords(records).catch((e)=>{ return Promise.reject(e) });
        if(response) {
            dispatch('getAll');
            return Promise.resolve(response);
        }
    },

};
