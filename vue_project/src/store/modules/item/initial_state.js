/**
* The initial values for the vuex state.
*/
export default {
	grid_headers: [     
		{
			sortable: true,
			text: 'product_name',
			value: 'name'
        },
        
		{
			sortable: true,
			text: 'quantity',
			value: 'quantity',
            align: 'right',
            width:'20%'
        },

        {
			sortable: true,
			text: 'unit_cost',
			value: 'format_unit_cost',
            align: 'right',
            width:'20%'
        },

        {
			sortable: true,
			text: 'total_amount',
			value: 'total_amount',
            align: 'right',
            width:'20%'
        },

        {
			text: 'actions',
			value: 'actions',		
            align: 'left',
            sortable: false,
            width:'5%'
		},        
    ],

    grid_config: {
        sortBy: [],
        sortDesc: [],
        page: 1,
        itemsPerPage: 15,
        mustSort: false,
        multiSort: true,
        rowsPerPageItems: [50, 100, 150, 200, 300, 400],
        static_filters: {
            search: '',
            selected_status: [],
        },
        dynamic_filters: [
            {
                field: 'quantity',
                value: null,
                mode: '>=',
                type: 'number'
            },

            {
                field: 'quantity',
                value: null,
                mode: '<=',
                type: 'number'
            },

            {
                field: 'unit_cost',
                value: null,
                mode: '>=',
                type: 'number'
            },

            {
                field: 'unit_cost',
                value: null,
                mode: '<=',
                type: 'number'
            }  
        ]
    },

    form: {
        id: 0,
        name: '',
        quantity: '',
        unit_cost: ''
    }
};
