
export const mutations = {
    loadEntity (state, entity) {
        state.form = entity;
    },

    setRecords (state, records) {
        state.records_config.records = records.results;
        state.records_config.total_rows = records.count;
    },

    setGridGeneralConfig (state, config) {
        state.records_config.general_config = config;
    },

    cleanRecords (state) {
        state.records_config.records = [];
    },

    setSelectedRecords (state, records) {
        console.log(records);
        state.records_config.selected = records;
    },

    purgeInformation (state) {
        state.records_config.records = [];
        state.records_config.total_rows = 0;
    },
};

export const actions = {
    async getAll ({commit, state}, grid_config) {
        var search_params = Object.clone(grid_config || state.search_params);
        
        commit('setGridGeneralConfig', search_params);        
        const response = await state.API.getAll(search_params).catch((e)=>{ return Promise.reject(e) });
        
        //Setting records_set
        commit('setRecords', response);
        return Promise.resolve(response);
    },

    async get ({commit, state}, id) {
        //Getting record
        const response = await state.API.get(id).catch((e)=>{ return Promise.reject(e) });

        if(response){
            commit('loadEntity', response);
            return Promise.resolve(response);
        }
    },

    async save ({ state }) {
        //Saving record
        const response = await state.API.save(state.form).catch((e)=>{ return Promise.reject(e) });
        if(response) return Promise.resolve(response);
    },

    async deleteRecords ({ dispatch, state }) {
        const record_ids = state.records_config.selected.map((e)=>{ return e.id });
        //Deleting record
        const response = await state.API.deleteRecords(record_ids).catch((e)=>{ return Promise.reject(e) });
        if(response) {
            dispatch('getAll');
            return Promise.resolve(response);
        }
    }
}