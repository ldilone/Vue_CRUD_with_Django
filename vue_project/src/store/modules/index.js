const required_modules = require.context(".", true, /index.js$/); //extract js files inside modules folder
const modules = {};

required_modules.keys().forEach(file_name => {
    const folders = file_name.split('/');
    const module_name = folders.slice(folders.length -2, folders.length -1)[0];

    if(module_name == '.') return;

    modules[module_name] = required_modules(file_name).default;
});

export default modules;