
// eslint-disable-next-line
const isAuthorized = (user_roles, required_module) => {
    if(!required_module.meta || !required_module.meta.roles || !required_module.meta.roles.length) return true;

    if(!user_roles || !user_roles.length) return false;

    //Getting user access for required module
    var allowed_roles = required_module.meta.roles.filter((e)=>{
        return user_roles.indexOf(e) > -1;
    });

    return allowed_roles.length;
}

const checkUserAuth = (to, from, next, store) => {
    const SESSION_TOKEN = store.state.auth.session_token;
    const USER_ROLES = store.state.auth.logged_user.roles;

    if(!SESSION_TOKEN && (to.path != '/login'))
        next('/login');        
    else{
        //Avoid to enter login when the user is logged
        if(SESSION_TOKEN && to.path == '/login') {
            next('/admin');
            return;
        }

        if (to.matched.some( record => !isAuthorized(USER_ROLES, record) )) {
            //USer does not have access to the specific module
            next('/admin');
            return;
        }

        next();
    }
}

const menuValidator = (menu_options, router_menu_options, user_roles) => {
    let options_with_roles = {};
    
    const __hasRole = (element, search_roles) => {
        return (element['children'] && !element['name']) || //If option has no name associated
                user_roles.find((e)=>{ return search_roles.indexOf(e) > -1 }) //Looking for menu roles
    }

    const __getMenuOptions = (options) => {
        //Getting only menu options associated with user roles
        options = options.filter((e)=>{
            let option_roles = options_with_roles[e.name] || [];
            return !option_roles.length || __hasRole(e, option_roles); 
        });

        //Searching for children options
        options.forEach((e, index, array) => {
            if(e.children)
                array[index].children = __getMenuOptions(e.children);
        });
    
        return options;
    }

    const __getRouterOptionsRoles = (options, acumulator) => {   
        options.forEach((e) => {
            acumulator[e.name] = e.meta && e.meta.roles ? e.meta.roles : [];
    
            if(e.children){
                //Getting all roles associated to module children
                var children_roles = e.children.map((el)=>{
                    return el.meta && el.meta.roles ? el.meta.roles : [];
                });
    
                //Adding roles to the parent
                acumulator[e.name] = Array.prototype.concat.apply([], children_roles); 
                //Getting children menu roles
                __getRouterOptionsRoles(e.children, acumulator);
            }
        });
    }

    __getRouterOptionsRoles(router_menu_options, options_with_roles);
    
    return {
        getMenu () {
            return __getMenuOptions(menu_options);
        }
    } 
}


export default {
    checkUserAuth: checkUserAuth,
    menuValidator: menuValidator
}