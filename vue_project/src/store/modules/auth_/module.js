import menu_options from './menu_options'
import API from './api'
import security from "./router_security"
import router from '../../../router'

const TOKEN_REFRESH_INTERVAL = 30; //Interval in seconds
var timer = null;

export const state = {
    user_menu_options: [],
    logged_user: JSON.parse(localStorage.getItem('user')) || {},
    session_token: JSON.parse(localStorage.getItem('session_token')) || null
};

export const getters = {

};

export const mutations = {
    setMenuOptions(state, menu_options) {
        state.user_menu_options = menu_options;
    },

    setLoggedUser(state, login_data) {
        const user = {
            username: login_data.username,
            roles: login_data.roles
        };

        const token = {
            refresh: login_data.refresh,
            access: login_data.access
        };

        localStorage.setItem('user', JSON.stringify(user));
        localStorage.setItem('session_token', JSON.stringify(token));
        state.logged_user = user;
        state.session_token = token;
    },

    logoutUser(state) {
        localStorage.removeItem('user');
        localStorage.removeItem('session_token');
        state.logged_user = {};
    },

    setAccessToken(state, token) {
        state.session_token.access = token.access;
        localStorage.setItem('session_token', JSON.stringify(state.session_token));
    },

};

export const actions = {
    getMenuOptions({commit}) {
        commit('setMenuOptions', menu_options.MENU);
    },

    drawMenu ({state, commit}, menu_options) {
        var menu = security.menuValidator(
            menu_options,
            router.options.routes, 
            state.logged_user.roles
        ).getMenu();
        
        commit('setMenuOptions', menu);
    },

    /**
     * Refresh token method
     * @param {*} refresh_token - storage token used to refresh access token
     */
    async refreshToken({ dispatch, commit}, refresh_token) {
        const response = await API.refreshToken({
            refresh: refresh_token
        }).catch(() => {});

        if (!response) {
            dispatch('logoutUser');
            return;
        }

        commit('setAccessToken', response);
    },

    /**
     * Managin refresh token
     */
    scheduleRefresh({ dispatch, state }) {
        if (!state.session_token) return;

        timer = setInterval(() => {
            var session_data = state.session_token;
            //Stop refresh token if user is not logged
            if (!session_data.refresh) {
                clearInterval(timer);
                dispatch('logoutUser');
                window.location = '/login';
                return;
            }

            dispatch('refreshToken', session_data.refresh);
        }, TOKEN_REFRESH_INTERVAL * 1000);
    },

    async login({ commit, dispatch }, user) {
        const response = await API.login(user).catch(() => {});

        if (!response || !response.access)
            return Promise.reject(false);

        commit('setLoggedUser', response);
        dispatch('drawMenu', menu_options.MENU);
        return Promise.resolve(true);
    },

    logOut({ commit }) {
        commit('logoutUser')
        window.location = '/login';
        return;
    },

    loadInitialData({ dispatch }) {
        if(state.session_token){
            //dispatch('refreshToken', state.session_token.refresh);
            dispatch('drawMenu', menu_options.MENU);
            dispatch('scheduleRefresh');
        }
    },

};