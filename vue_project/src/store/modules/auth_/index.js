import * as auth from './module';

const state = {
   ...auth.state,
};

const getters = {
   ...auth.getters,
};

const actions = {
    ...auth.actions,
};

const mutations = {
    ...auth.mutations,
};

export default {
   namespaced: true,
   state,
   getters,
   actions,
   mutations
};