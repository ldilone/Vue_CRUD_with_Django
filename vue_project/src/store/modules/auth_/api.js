import http_request from '@/store/api'

const ENDPOINT = http_request.default_endpoint;

export default {
    login (user) {
        return http_request.post(ENDPOINT + 'api/login/', user);        
    },

    refreshToken (token) {
        return http_request.post(ENDPOINT + 'api/token/refresh/', token);        
    },

    changePassword: (user_data) => {
		return http_request.post(ENDPOINT + 'api/access/change_password/', user_data);
    },    
}
