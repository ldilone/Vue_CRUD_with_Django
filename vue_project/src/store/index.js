import Vue from 'vue'
import Vuex from 'vuex'
import modules from "./modules"

Vue.use(Vuex)

export default new Vuex.Store({
	modules: {
		...modules
    },
        
	state: {

	},

	mutations: {
		initialiseStore(state) {
			//This implementation is just 
		},
		
		changeStateValue(state, property_data) {
			state[property_data.module][property_data.property_name] = property_data.value;
        },
        
        addVisitedModule (state, module) {
            state.visited_modules.push(module);
        },

        removeVisitedModule (state, module) {
            const index = state.visited_modules.findIndex(e => e == module);
            state.visited_modules.splice(index, 1);
        }
	},

	actions: {
		initialiseStore () {
			this.dispatch('auth/loadInitialData');
        },

        chageFormElementValue (context, element) {
			context.commit(element.method, element.value);
		},

		showMessage ({commit}, message_object) {
			commit('system/setMessage', message_object)
		},

		showDialog ({commit}, message_object) {
			commit('system/setDialogMessage', message_object)
		},

		showConfirm ({commit}, message_object) {
			commit('system/setConfirm', message_object)
        },
        
        registerModules ({ state, commit }, modules) {
            state.visited_modules.map((e)=>{
                if(!modules.find(s => s.name == e)) {
                    this.commit(e + '/purgeInformation');
                    commit('removeVisitedModule', e);
                }
            });

            modules.map((e)=>{
                if(!this.state[e.name]){
                    this.registerModule(e.name, e.module);
                    commit('addVisitedModule', e.name);
                }
            })
        }
	},
})
