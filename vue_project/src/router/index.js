import Vue from "vue"
import Router from "vue-router"

Vue.use(Router);

let router = new Router({
    base: process.env.BASE_URL,
    routes: [
        //Menu for admin page
        {
            path: "/login",
            name: "login",
            component: () => import('@/views/login/Login.vue'),
        },

        //Admin views
        {
            path: "/",
            name: "admin",
            component: () => import('@/layouts/Admin.vue'),
            children: [
                {
                    path: "/", name: "item_grid",
                    component: () => import('@/views/admin/item/Grid.vue')
                },              
                {
                    path: "editorcrate/:id?", name: "item_form",
                    component: () => import('@/views/admin/item/Form.vue')
                },     

            ]
        },

        {
            path: '*',
            name: '404_page',
            component: () => import('@/views/404/404_page')     
        }
    ]
});

export default router;
