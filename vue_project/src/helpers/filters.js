import moment from 'moment'

const setGlobalFilters = (VueApp) => {
    VueApp.filter('format_date', function (value) {
        if(value == '0000-00-00') return '00-00-0000';
        return value ? moment(value).format('DD-MM-YYYY') : '';
    });
    
    VueApp.filter('currency', function (value) {
        return parseFloat(value || 0.00).format();
    });
}

export default {
	setGlobalFilters: setGlobalFilters 
}
