const CURRENT_MODULE = 'item';
import {mapActions, mapMutations, mapGetters } from 'vuex'

export default {
    computed: {

    },

    methods: {
        ...mapActions({
            saveEntity: CURRENT_MODULE + '/save',
            get: CURRENT_MODULE + '/get',
            getAll: CURRENT_MODULE + '/getAll',
            deactivateRecords: CURRENT_MODULE + '/deactivateRecords',
        }),

        ...mapGetters(CURRENT_MODULE, ['getInitialFilters']),

        ...mapMutations ({
            clearForm: CURRENT_MODULE + '/clearForm',
            setSelectedRecords: CURRENT_MODULE + '/setSelectedRecords'
        }),

        validate () {
            if(!this.$refs.form.validate()){
                this.system.showMessage('FILL_FORM');
                return false;
            }

            if(this.form.quantity <= 0){
                this.system.showMessage('QTY_GREATER_THAN_CERO');
                return false;
            }

            if(this.form.unit_cost <= 0){
                this.system.showMessage('UNIT_COST_GREATER_THAN_CERO');
                return false;
            }

            return true;
        },

        async save () {
            //Avoid to save when there are errors or is still saving
            if(!this.validate() || this.saving) return false;

            this.saving = true;
            const result = await this.saveEntity().catch((e)=>{  });            
            this.system.showMessage(result ? 'SAVE_SUCCESS' : 'SAVE_ERROR');
            
            if(result) {
                this.clearForm();
                this.$refs.form.resetValidation();
                document.getElementById('product_name').focus();
                this.getRecords();
            }

            this.saving = false;
        },

        async getRecords (pagination_data) {
            this.loading = true;
            const result = await this.getAll(pagination_data).catch((e)=>{ this.logError(e) });
            this.loading = false;
            if(!result) this.system.showMessage('ERROR_LOADING_DATA');
        },
        
        async deactivateRecord () {
            const records_qty = this.records_config.selected.length;
            if(!records_qty){
                this.system.showMessage('SELECT_ONE_RECORD');
                return;
            }

            const option = await this.system.confirm('DEACTIVATE_CONFIRM');
            if(!option) return;
            
            const result = await this.deactivateRecords().catch((e)=>{ this.logError(e) });            
            this.system.showMessage(result 
                ? (records_qty == 1 ? 'DEACTIVATE_SUCCESS' : 'DEACTIVATE_SUCCESS_PLURAL') 
                : 'DEACTIVATE_ERROR'
            );
        },

        async editRecord (row) {
           await this.get(row.id);
           this.show_filter = true;
        },

        cleanFilters () {
            this.$refs.grid_view.cleanFilters(this.getInitialFilters());
        }
    },

}
